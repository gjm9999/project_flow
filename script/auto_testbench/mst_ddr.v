// +FHDR------------------------------------------------------------
//                 Copyright (c) 2022 .
//                       ALL RIGHTS RESERVED
// -----------------------------------------------------------------
// Filename      : sram_common.v
// Author        : 
// Created On    : 2022-08-22 10:59
// Last Modified : 2022-09-14 04:24 by xiaotu
// -----------------------------------------------------------------
// Description:
//
//
// -FHDR------------------------------------------------------------

module mst_ddr #(
    parameter CNH_NUM = 8 , 
    parameter USER_WD = 32,
    parameter COMMON_WD   = 14,
    parameter BANK_WD= 3
)(/*AUTOARG*/
   // Outputs
   common_conv_posi_ready, common_loop_cal_ready,
   common_conv_cal_ready, sram_ctrl_posi_valid, sram_ctrl_posi,
   sram_ctrl_posi_index, sram_ctrl_posi_last, sram_ctrl_send_valid,
   sram_ctrl_send_last, sram_ctrl_send_strb, sram_ctrl_send,
   sram_ctrl_send_index, sram_ctrl_cal_valid, sram_ctrl_cal,
   sram_ctrl_cal_posi_parity, sram_ctrl_cal_wid_parity,
   sram_ctrl_cal_strb, sram_ctrl_cal_last,
   // Inputs
   clk, rst_n, cfg_channel_num, cfg_out_width, conv_common_posi_valid,
   conv_common_posi, conv_common_posi_user, conv_common_posi_last,
   loop_common_cal_valid, loop_common_cal_strb, loop_common_send,
   loop_common_send_user, loop_common_cal_last, conv_common_cal_valid,
   conv_common_cal_strb, conv_common_cal_posi,
   conv_common_cal_posi_parity, conv_common_wid, conv_common_cal_last,
   ctrl_sram_posi_ready, ctrl_sram_send_ready, ctrl_sram_cal_ready
   );

//***************************************************
//interface
//***************************************************
input          clk;
input          rst_n;
input  [8 -1:0]cfg_channel_num;
input  [4 -1:0]cfg_out_width;

input                 conv_common_posi_valid;
output                common_conv_posi_ready;
input  [COMMON_WD-1:0]conv_common_posi;
input  [BANK_WD  -1:0]conv_common_posi_user;
input                 conv_common_posi_last;

input                         loop_common_cal_valid;
output                        common_loop_cal_ready;
input  [CNH_NUM          -1:0]loop_common_cal_strb;
input  [COMMON_WD*CNH_NUM -1:0]loop_common_send;
input  [USER_WD*CNH_NUM  -1:0]loop_common_send_user;
input                         loop_common_cal_last;

input                  conv_common_cal_valid;
output                 common_conv_cal_ready;
input  [CNH_NUM   -1:0]conv_common_cal_strb;
input  [COMMON_WD -1:0]conv_common_cal_posi;
input  [BANK_WD   -1:0]conv_common_cal_posi_parity;
input  [5*CNH_NUM -1:0]conv_common_wid;
input                  conv_common_cal_last;

output                sram_ctrl_posi_valid; 
input                 ctrl_sram_posi_ready;
output [COMMON_WD-1:0]sram_ctrl_posi;
output [USER_WD  -1:0]sram_ctrl_posi_index; 
output                sram_ctrl_posi_last;

//out send
output                        sram_ctrl_send_valid; 
input                         ctrl_sram_send_ready; 
output                        sram_ctrl_send_last; 
output [CNH_NUM          -1:0]sram_ctrl_send_strb; 
output [COMMON_WD*CNH_NUM-1:0]sram_ctrl_send;       
output [USER_WD*CNH_NUM  -1:0]sram_ctrl_send_index; 

//out cal
output                  sram_ctrl_cal_valid;
input                   ctrl_sram_cal_ready;
output [32*CNH_NUM -1:0]sram_ctrl_cal;
output [BANK_WD    -1:0]sram_ctrl_cal_posi_parity;
output [CNH_NUM    -1:0]sram_ctrl_cal_wid_parity;
output [CNH_NUM    -1:0]sram_ctrl_cal_strb;
output                  sram_ctrl_cal_last;
//***************************************************
//interface over
//***************************************************

//***************************************************
//AOTOGRN
//***************************************************
/*AUTOWIRE*/
//***************************************************
//AOTOGRN over
//***************************************************

endmodule
// Local Variables:
// verilog-auto-inst-param-value:t
// verilog-library-directories:("." "../../cbb/pipe" "../../cbb/misc")
// verilog-library-extensions:(".v")
// End:
