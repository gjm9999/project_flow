setenv PATH "${PATH}:${PROJ_ROOT}/script"
setenv PATH "${PATH}:${PROJ_ROOT}/script/spyg_rtl"
setenv PATH "${PATH}:${PROJ_ROOT}/script/auto_testbench"

alias cmp "${PROJ_ROOT}/script/shutcut_proc -c"
alias vd "${PROJ_ROOT}/script/shutcut_proc -v"
alias sg "${PROJ_ROOT}/script/shutcut_proc -s"

alias clean "rm -rf novas.conf  novas.rc  verdiLog  *.log  ./spyglass"
